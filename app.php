<?php
namespace RestApi;

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Symfony\Component\Dotenv\Dotenv;

require_once 'vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

$containerBuilder = new ContainerBuilder();

$dependencies = require __DIR__ . '/app/dependencies.php';
$dependencies($containerBuilder);

$repositories = require __DIR__ . '/app/repositories.php';
$repositories($containerBuilder);

AppFactory::setContainer($containerBuilder->build());
$app = AppFactory::create();

$middleware = require __DIR__ . '/app/middleware.php';
$middleware($app);

$routes = require __DIR__ . '/app/routes.php';
$routes($app);

$app->addBodyParsingMiddleware();

$app->run();
