# syntax=docker/dockerfile:1.2

FROM composer:2.3 AS base
WORKDIR /tmp/www
COPY composer.json composer.lock ./ 
RUN composer install --no-dev
COPY src/ src
RUN composer dumpautoload

FROM php:8.0-cli-alpine AS final
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql
WORKDIR /var/www
COPY app/ app
COPY src/ src
COPY --from=base /tmp/www/vendor/ vendor
COPY .env .
COPY app.php .

EXPOSE 80/tcp

ENTRYPOINT [ "php", "-S", "0:80", "app.php" ]
