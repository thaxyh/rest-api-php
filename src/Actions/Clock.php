<?php
namespace Api\Actions;

use Fig\Http\Message\StatusCodeInterface as HttpStatus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Response;

class Clock implements RequestHandlerInterface
{

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $response = new Response();
        $data = [
            'clock' => date(DATE_W3C),
        ];

        $response
            ->getBody()
            ->write(json_encode($data));

        return $response
            ->withStatus(HttpStatus::STATUS_OK)
            ->withHeader('Content-Type', 'application/json');
    }

}