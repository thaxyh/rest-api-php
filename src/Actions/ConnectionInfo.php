<?php
namespace Api\Actions;

use Api\Repositories\VisitsCounter;
use Fig\Http\Message\StatusCodeInterface as HttpStatus;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Response;

class ConnectionInfo implements RequestHandlerInterface
{

    private LoggerInterface $logger;
    private VisitsCounter $visitsCounterRepository;

    public function __construct(LoggerInterface $logger, VisitsCounter $visitsCounter)
    {
        $this->logger = $logger;
        $this->visitsCounterRepository = $visitsCounter;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $env = getenv();
        ksort($env);

        $response = new Response();
        $data = [
            'hostname' => gethostname(),
            'remoteAddr' => $_SERVER['REMOTE_ADDR'] ?? null,
            'remotePort' => $_SERVER['REMOTE_PORT'] ?? null,
            'httpHost' => $_SERVER['HTTP_HOST'] ?? null,
            'serverPort' => $_SERVER['SERVER_PORT'] ?? null,
            'visits' => $this->visitsCounterRepository->fetchVisits(),
        ];

        $response
            ->getBody()
            ->write(json_encode($data));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(HttpStatus::STATUS_OK);
    }

}