<?php
namespace Api\Repositories;

use Api\Database;
use Psr\Http\Message\ServerRequestInterface;
use PDO;

class VisitsCounter
{

    const TABLE_NAME = 'visits';

    private Database $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function incrementVisits(ServerRequestInterface $request): void
    {
        $db = $this->database->getConnection();

        $db->query('UPDATE ' . self::TABLE_NAME . ' SET counter = counter + 1');
    }

    public function fetchVisits(): int
    {
        $db = $this->database->getConnection();
        $query = $db->query('SELECT counter FROM ' . self::TABLE_NAME . ' LIMIT 1');

        if ($query->rowCount() > 0) {
            return $query->fetch(PDO::FETCH_ASSOC)['counter'] ?? 0;
        }
        else {
            return 0;
        }
    }
}