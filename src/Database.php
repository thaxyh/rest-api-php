<?php
namespace Api;

use Api\Repositories\VisitsCounter as VisitsCounterRepository;
use PDO;
use Psr\Log\LoggerInterface;
use Throwable;

class Database
{

    private LoggerInterface $logger;
    private ?PDO $connection = null;

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;

        return $this;
    }

    public function schemaGracefulCreate(): self
    {
        try {
            $db = $this->getConnection();
        }
        catch (Throwable $ex) {
            $this->logger->error($ex);

            return $this;
        }

        $visitsTable = VisitsCounterRepository::TABLE_NAME;
        $visitsTableExists = $db->query("SHOW TABLES LIKE '{$visitsTable}'")->rowCount() > 0;

        if (!$visitsTableExists) {
            $db->query(<<<SQL
                CREATE TABLE {$visitsTable} (
                    counter INT DEFAULT 0 NOT NULL UNIQUE
                )
            SQL);

            $db->query(<<<SQL
                INSERT INTO {$visitsTable}
                    (counter)
                VALUES
                    (0)
            SQL);

            $this->logger->info('Schema created successfully.');
        }

        return $this;
    }

    public function getConnection(): PDO
    {
        if (!is_null($this->connection)) {
            return $this->connection;
        }

        $url = $_ENV['DATABASE_URL'] ?? '';

        $this->connection = new PDO(
            $url,
            null,
            null,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );

        return $this->connection;
    }

}