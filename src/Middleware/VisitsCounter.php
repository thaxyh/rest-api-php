<?php
namespace Api\Middleware;

use Api\Repositories\VisitsCounter as VisitsCounterRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class VisitsCounter implements MiddlewareInterface
{

    private VisitsCounterRepository $visitsCounterRepository;

    public function __construct(VisitsCounterRepository $visitsCounterRepository)
    {
        $this->visitsCounterRepository = $visitsCounterRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->visitsCounterRepository->incrementVisits($request);

        return $handler->handle($request);
    }

}