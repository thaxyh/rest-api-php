<?php

use Api\Middleware\VisitsCounter;
use Slim\App;

return function (App $app) {
    $app->add(VisitsCounter::class);
};
