<?php

use Api\Database;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Logger;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            return new Logger();
        },
        Database::class => function(ContainerInterface $c) {
            $db = new Database();
            $db
                ->setLogger($c->get(LoggerInterface::class))
                ->schemaGracefulCreate();

            return $db;
        },
    ]);
};
