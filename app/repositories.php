<?php
declare(strict_types=1);

use Api\Repositories\VisitsCounter;
use DI\ContainerBuilder;
use function DI\autowire;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        VisitsCounter::class => autowire(VisitsCounter::class),
    ]);
};
