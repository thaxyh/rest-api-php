<?php

use Api\Actions\Clock;
use Api\Actions\ConnectionInfo;
use Slim\App;

return function (App $app) {
    $app->get('/', ConnectionInfo::class);
    $app->get('/clock', Clock::class);
};
